package jp.co.sizuha.atelier.aninow.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import jp.co.sizuha.atelier.aninow.R
import jp.co.sizuha.atelier.aninow.config.usePermissions
import jp.co.sizuha.atelier.aninow.model.Anime
import jp.co.sizuha.atelier.aninow.ui.adapter.AnimeAdapter
import kotlinx.android.synthetic.main.activity_main.*
import sizuha.library.slib.util.SystemUtil
import android.content.Intent
import sizuha.library.slib.util.Alert
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.view.View
import android.widget.ListView
import android.widget.Toast
import com.kishe.sizuha.kotlin.squery.SQuery
import jp.co.sizuha.atelier.aninow.App
import jp.co.sizuha.atelier.aninow.BackupManager
import jp.co.sizuha.atelier.aninow.config.USER_DB_VER
import jp.co.sizuha.atelier.aninow.model.AnimeSortOption
import jp.co.sizuha.atelier.aninow.model.MediaType
import jp.co.sizuha.atelier.aninow.ui.dialog.AnimeInfoDialog
import jp.co.sizuha.atelier.aninow.ui.dialog.AnimeSortDialog
import jp.co.sizuha.atelier.aninow.ui.dialog.WaitDialog
import kotlinx.android.synthetic.main.sub_settings.*
import sizuha.library.slib.util.Logger
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var appLocalData: SQuery

    enum class PageTab { NOW, FINISHED, SETTING }
    private var currentTab = PageTab.NOW

    private var animeListAdapter: AnimeAdapter? = null

    private val reqPermissions = 1000
    private val reqNewItem = 2000
    private val reqEditItem = 2001
    private val reqImportCsv = 3000

    private var sortOption = AnimeSortOption()

    private var actionBarMenu: Menu? = null
    private var actionSortOption: MenuItem? = null
    private var actionAddItem: MenuItem? = null

    fun getContext() = this

    //--- Lifecycle --------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        restoreInstanceState(savedInstanceState)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        initDB()

        val app = App.getInstance(this)
        sortOption.targetField = app.preference?.sortField ?: AnimeSortOption.Field.TITLE
        sortOption.asc = app.preference?.sortAsc ?: true


        animeListAdapter = AnimeAdapter(this)

        list_anime.adapter = animeListAdapter
        list_anime.choiceMode = ListView.CHOICE_MODE_SINGLE
        animeListAdapter?.onItemClickListener = OnListItemClick()

        action_import.setOnClickListener { showImportCsv() }
        action_export.setOnClickListener { showExportCsv() }
        action_reset_db.setOnClickListener { confirmResetDB() }

        val needRequest = SystemUtil.checkPermissions(this, usePermissions)
        SystemUtil.requestPermisions(this, needRequest, reqPermissions)

        text_app_ver.text = App.getVersion(this)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        restoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        reloadItems()
    }

    override fun onPause() {
        val app = App.getInstance(this)
        app.preference?.sortField = sortOption.targetField
        app.preference?.sortAsc = sortOption.asc

        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        sortOption.writeTo(outState)

        outState?.putInt("tab", currentTab.ordinal)

        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        appLocalData.close()
        super.onDestroy()
    }

    //--- End: Lifecycle ---------------------------------------------------------------------------


    //--- Activity Events --------------------------------------------------------------------------

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_tools, menu)

        actionBarMenu = menu
        actionSortOption = menu?.findItem(R.id.action_sort)
        actionAddItem = menu?.findItem(R.id.action_add_item)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_add_item -> showAddItem()
            R.id.action_sort -> showSortOptions()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != reqPermissions) return

        for (gr in grantResults) {
            if (gr != PackageManager.PERMISSION_GRANTED) {
                Alert.showError(this, getString(R.string.err_app_permissions), null)
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            reqNewItem -> {
                val item = data?.getParcelableExtra<Anime>("item")
                if (item != null) addItem(item)
                return
            }
            reqEditItem -> {
                val item = data?.getParcelableExtra<Anime>("item")
                if (item != null) {
                    saveItem(item)
                    reloadItems()
                }
                return
            }
            reqImportCsv -> {
                val filepath = data?.getStringExtra(OpenFileActivity.RES_FIELD_PULL_FILEPATH)
                if (filepath != null) {
                    val filename = data.getStringExtra(OpenFileActivity.RES_FIELD_FILENAME)
                    val msg = String.format(getString(R.string.msg_format_inport_confirm), filename)

                    Alert.confirm(this, msg) { _,_ ->
                        Logger.i("import: $filepath")
                        ImportTask(filepath).execute()
                    }
                }
                return
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        Alert.confirm(this, getString(R.string.msg_confirm_exit)) { _, _ ->
            finish()
        }
    }

    //--- End: Activity Events ---------------------------------------------------------------------


    private fun restoreInstanceState(savedInstanceState: Bundle?) {
        sortOption.load(savedInstanceState)

        val tabIndex = savedInstanceState?.getInt("tab") ?: 0
        currentTab = PageTab.values()[tabIndex]

        navigation.selectedItemId = when (currentTab) {
            PageTab.FINISHED -> R.id.navigation_fin
            PageTab.SETTING -> R.id.navigation_setting
            else -> R.id.navigation_now
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_now -> {
                if (currentTab != PageTab.NOW) {
                    currentTab = PageTab.NOW
                    reloadItems()
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_fin -> {
                if (currentTab != PageTab.FINISHED) {
                    currentTab = PageTab.FINISHED
                    reloadItems()
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                currentTab = PageTab.SETTING
                updateActionBar()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun initDB() {
        appLocalData = SQuery(this, "app_db", USER_DB_VER)
        appLocalData.createTable(Anime.tableName, Anime())
    }

    private fun updateActionBar() {
        when (currentTab) {
            PageTab.NOW -> {
                supportActionBar?.title = getString(R.string.txt_ing) + getSortInfoText()
                actionSortOption?.isVisible = true
                actionAddItem?.isVisible = true

                list_anime.visibility = View.VISIBLE
                settings.visibility = View.GONE
            }
            PageTab.FINISHED -> {
                supportActionBar?.title = getString(R.string.txt_fin) + getSortInfoText()
                actionSortOption?.isVisible = true
                actionAddItem?.isVisible = true

                list_anime.visibility = View.VISIBLE
                settings.visibility = View.GONE
            }
            PageTab.SETTING -> {
                supportActionBar?.title = getString(R.string.txt_setting)
                actionSortOption?.isVisible = false
                actionAddItem?.isVisible = false

                list_anime.visibility = View.GONE
                when_empty.visibility = View.GONE
                settings.visibility = View.VISIBLE
            }
        }
    }

    private fun getSortInfoText(): String =
        "（" + sortOption.getSortFieldText(this) +
                (if (sortOption.asc) getString(R.string.arrow_up) else getString(R.string.arrow_down)) + "）"

    private fun loadItemsAsync(isFinished: Boolean) {
        LoadItemsTask(isFinished).execute()
    }

    private fun reloadItems() {
        Logger.i("reloadItems")

        when (currentTab) {
            PageTab.NOW -> loadItemsAsync(false)
            PageTab.FINISHED -> loadItemsAsync(true)
            else -> updateActionBar()
        }
    }

    private fun removeItem(idx: Int) {
        appLocalData.from(Anime.tableName)
                .where("idx=?", idx)
                .delete()

        animeListAdapter?.items?.removeAll { item -> item.idx == idx }
        animeListAdapter?.notifyDataSetChanged()
    }

    private fun addItem(item: Anime) {
        appLocalData.from(Anime.tableName).insert(item)

        if (item.finished && currentTab != PageTab.FINISHED) return
        if (!item.finished && currentTab != PageTab.NOW) return

        reloadItems()
    }

    private fun saveItem(item: Anime) {
        appLocalData.from(Anime.tableName).update(item)
    }

    private fun showAddItem() {
        AnimeEditActivity.startActivityForResult(this, reqNewItem)
    }

    private fun showSortOptions() {
        AnimeSortDialog.show(this, sortOption) {
            reloadItems()
        }
    }

    private fun showItemEditor(item: Anime) {
        AnimeEditActivity.startActivityForResult(this, reqEditItem, item)
    }

    inner class OnListItemClick : AnimeAdapter.OnItemClick {
        override fun onItemClick(item: Anime, position: Int) {
            animeListAdapter?.closeAllItems()
            AnimeInfoDialog.show(getContext(), item) { item, applyNow ->
                if (applyNow) {
                    saveItem(item)
                    reloadItems()
                }
                else {
                    showItemEditor(item)
                }
            }
        }

        override fun onRemoveClick(item: Anime, position: Int) {
            Alert.confirm(getContext(), getString(R.string.msg_confirm_del)) { _,_ ->
                removeItem(item.idx)
                animeListAdapter?.closeItem(position)
            }
        }

        override fun onFinishedClick(item: Anime, position: Int) {
            when (currentTab) {
                PageTab.FINISHED -> item.finished = false
                PageTab.NOW -> item.finished = true
                else -> Unit // Nothing
            }

            saveItem(item)
            animeListAdapter?.closeItem(position)
            animeListAdapter?.items?.removeAll { it.idx == item.idx }
            animeListAdapter?.notifyDataSetChanged()
        }

        override fun onEditClick(item: Anime, position: Int) {
            showItemEditor(item)
            animeListAdapter?.closeItem(position)
        }
    }

    private fun showImportCsv() {
        Logger.i("import!")

        OpenFileActivity.startActivityForResult(this, reqImportCsv, getString(R.string.msg_import_target), listOf("csv"))
    }

    @SuppressLint("SimpleDateFormat")
    private fun showExportCsv() {
        Logger.i("export!")

        Alert.confirm(this, getString(R.string.msg_confirm_backup)) { _, _ ->
            val nameFormat = SimpleDateFormat("yyyyMMdd-HHmmss")
            val outFile = "aninow-" + nameFormat.format(Date()) + ".csv"

            File(App.externalExportPath).mkdirs()
            ExportTask(App.externalExportPath + outFile).execute()
        }
    }

    private fun confirmResetDB() {
        Alert.confirm(this, getString(R.string.msg_confirm_reset_db)) { _,_ ->
            appLocalData.from(Anime.tableName).delete()

            Alert.popup(getContext(), getString(R.string.msg_done), null)
        }
    }



    //--- Tasks ------------------------------------------------------------------------------------

    inner class LoadItemsTask(private val isFinished: Boolean) : AsyncTask<Void, Void, Boolean>() {

        private var mDlg: AlertDialog? = null

        override fun onPreExecute() {
            animeListAdapter?.items?.clear()
            mDlg = WaitDialog.show(getContext())
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            val argFin = if (isFinished) "1" else "0"
            val loadItems = appLocalData.from(Anime.tableName)
                    .where("fin=?", argFin)
                    .orderBy(sortOption.asDbColumn, sortOption.asc)
                    .orderBy("title", true)
                    .orderBy("title_other", true)
                    .select { Anime() }

            if (!isCancelled) {
                animeListAdapter?.items = loadItems
            }

            return true
        }

        override fun onPostExecute(result: Boolean?) {
            animeListAdapter?.notifyDataSetChanged()
            animeListAdapter?.closeAllItems()

            val isEmpty = animeListAdapter?.items?.isEmpty() ?: true
            when_empty.visibility = if (isEmpty) View.VISIBLE else View.GONE
            list_anime.visibility = if (isEmpty) View.GONE else View.VISIBLE

            updateActionBar()

            mDlg?.dismiss()
        }

        override fun onCancelled() {
            mDlg?.dismiss()
            super.onCancelled()
        }

    }


    inner class ImportTask(private val filepath: String) : AsyncTask<Void, Void, Boolean>() {

        private var mdlg: AlertDialog? = null

        override fun onPreExecute() {
            mdlg = WaitDialog.show(getContext())
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            return BackupManager().importCSV(appLocalData, filepath)
        }

        override fun onPostExecute(result: Boolean?) {
            mdlg?.dismiss()

            if (result == true) {
                Toast.makeText(getContext(), getString(R.string.msg_done), Toast.LENGTH_LONG).show()
                //Alert.popup(context, getString(R.string.msg_done), null)
            }
            else {
                Alert.showError(getContext(), getString(R.string.err_fail_load_csv), null)
            }
        }

        override fun onCancelled() {
            mdlg?.dismiss()
            super.onCancelled()
        }

    }

    inner class ExportTask(private val filepath: String) : AsyncTask<Void, Void, Boolean>() {

        private var mDlg: AlertDialog? = null

        override fun onPreExecute() {
            mDlg = WaitDialog.show(getContext())
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            return BackupManager().exportCSV(appLocalData, filepath)
        }

        override fun onPostExecute(result: Boolean?) {
            // Android OSがわにファイルをIndexにとうろくする
            // こうしないと、PC側ですぐには見えない
            val contentUri = Uri.fromFile(File(filepath))
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri)
            getContext().sendBroadcast(mediaScanIntent)
//            MediaScannerConnection.scanFile(context, arrayOf(App.externalExportPath), null) { path, uri ->
//
//            }

            mDlg?.dismiss()

            if (result == true) {
                Toast.makeText(getContext(), getString(R.string.msg_done), Toast.LENGTH_LONG).show()
                //Alert.popup(context, getString(R.string.msg_done), null)
            }
            else {
                Alert.showError(getContext(), getString(R.string.err_fail_export_csv), null)
            }
        }

        override fun onCancelled() {
            mDlg?.dismiss()
            super.onCancelled()
        }

    }

}
