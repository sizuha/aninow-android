package jp.co.sizuha.atelier.aninow.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import jp.co.sizuha.atelier.aninow.R
import kotlinx.android.synthetic.main.adapter_simple_text.view.*


class MonthAdapter(val context: Context) : BaseAdapter() {

    private val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val month = getItemId(position)

        val itemView = convertView ?: inflater.inflate(R.layout.adapter_simple_text, parent, false)

        itemView.text_display.text =
                if (month > 0)
                    month.toString() //+ context.getString(R.string.month)
                else
                    context.getString(R.string.txt_none_data)

        return itemView
    }

    override fun getItem(position: Int): Any = getItemId(position)

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = 13

}