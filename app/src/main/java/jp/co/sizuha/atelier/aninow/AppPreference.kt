package jp.co.sizuha.atelier.aninow

import android.content.Context
import jp.co.sizuha.atelier.aninow.model.AnimeSortOption

class AppPreference(context: Context) {

    private val pref = context.getSharedPreferences("app_pref", Context.MODE_PRIVATE)

    var sortField: AnimeSortOption.Field
        get() =
            AnimeSortOption.Field.values()[pref.getInt("sort_field", 0)]

        set(value) {
            val edit = pref.edit()
            edit.putInt("sort_field", value.ordinal)
            edit.commit()
        }

    var sortAsc: Boolean
        get() = pref.getBoolean("sort_asc", true)
        set(value) {
            val edit = pref.edit()
            edit.putBoolean("sort_asc", value)
            edit.commit()
        }

}