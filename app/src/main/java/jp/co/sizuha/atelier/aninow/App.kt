package jp.co.sizuha.atelier.aninow

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Environment
import jp.co.sizuha.atelier.aninow.config.APP_TAG
import jp.co.sizuha.atelier.aninow.config.SHOW_DEBUG_LOG
import sizuha.library.slib.SLibConfig

class App : Application() {

    companion object {

        fun getInstance(activity: Activity) =
            activity.application as App

        val externalStoragePath
            get() = Environment.getExternalStorageDirectory().absolutePath + "/"

        val externalAppStoragePath
            get() = externalStoragePath + "aninow/"

        val externalExportPath
            get() =
                externalAppStoragePath
                //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath + "/"

        fun getVersion(context: Context): String {
            val pkgInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            return pkgInfo.versionName + "." + pkgInfo.versionCode
        }
    }

    var preference: AppPreference? = null
        private set

    override fun onCreate() {
        super.onCreate()

        val logLevel = if (SHOW_DEBUG_LOG) SLibConfig.Debug.LogLevel.DEBUG else SLibConfig.Debug.LogLevel.INFO
        SLibConfig.init(SLibConfig.Config(APP_TAG, logLevel))

        com.kishe.sizuha.kotlin.squery.Config.enableDebugLog = SHOW_DEBUG_LOG

        preference = AppPreference(this)
    }



    val version: String
        get() = getVersion(this)

}