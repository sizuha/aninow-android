package jp.co.sizuha.atelier.aninow.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import jp.co.sizuha.atelier.aninow.R
import kotlinx.android.synthetic.main.adapter_simple_text.view.*
import sizuha.library.slib.util.Logger


class YearAdapter(val context: Context, val minYear: Int, val maxYear: Int) : BaseAdapter() {

    private val inflater = LayoutInflater.from(context)

    init {
        Logger.d("minYear: " + minYear)
        Logger.d("maxYear: " + maxYear)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        Logger.d("getView position: " + position)

        val year = getItemId(position)
        Logger.d("year: " + year)

        val itemView = convertView ?: inflater.inflate(R.layout.adapter_simple_text, parent, false)

        itemView.text_display.text =
                if (year > 0)
                    year.toString() //+ context.getString(R.string.year)
                else
                    context.getString(R.string.txt_none_data)

        return itemView
    }

    override fun getItem(position: Int): Any = getItemId(position)

    override fun getItemId(position: Int): Long =
        if (position <= 0) 0 else maxYear - (position-1L)

    override fun getCount(): Int = maxYear - minYear + 2

    fun getItemPosition(year: Int): Int =
        when (year) {
            0 -> 0
            in minYear..maxYear -> maxYear-year + 1
            else -> -1
        }

}