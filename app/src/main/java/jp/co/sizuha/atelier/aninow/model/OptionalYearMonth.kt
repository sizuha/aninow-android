package jp.co.sizuha.atelier.aninow.model

import java.util.*

class OptionalYearMonth {

    var year: Int = 0
    var month: Int = 0

    constructor() {
        year = 0
        month = 0
    }

    constructor(rawVal: Int) {
        from(rawVal)
    }

    constructor(year: Int, month: Int) {
        this.year = year
        this.month = month
    }

    override fun toString(): String {
        return String.format("%04d%02d", year, month)
    }

    fun toFormattedString(formatYear: String, formatMonth: String): String {
        if (isEmpty) return ""

        var result = ""
        if (year > 0) {
            result = String.format(formatYear, year)
        }

        if (month > 0) {
            result += String.format(formatMonth, month)
        }

        return result
    }

    fun toInt(): Int = year*100 + month

    val isEmpty: Boolean
        get() = year == 0 && month == 0


    fun from(calendar: Calendar): OptionalYearMonth {
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH) + 1
        return this
    }

    fun from(value: Int): OptionalYearMonth {
        if (value < 100) {
            year = 0
            month = value
        }
        else {
            year = value / 100
            month = value - year*100
        }
        return this
    }

    override fun equals(other: Any?): Boolean =
            if (other is OptionalYearMonth) {
                toInt() == other.toInt()
            }
            else super.equals(other)

}