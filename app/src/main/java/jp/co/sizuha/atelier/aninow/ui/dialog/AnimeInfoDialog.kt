package jp.co.sizuha.atelier.aninow.ui.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.EditText
import android.widget.RatingBar
import android.widget.TextView
import jp.co.sizuha.atelier.aninow.R
import jp.co.sizuha.atelier.aninow.model.Anime
import jp.co.sizuha.atelier.aninow.ui.minusProgressEpisode
import jp.co.sizuha.atelier.aninow.ui.plusProgressEpisode
import java.text.DecimalFormat


class AnimeInfoDialog {

    companion object {

        private var isChanged = false

        fun show(context: Context, item: Anime, onEdit: (item: Anime, applyNow: Boolean) -> Unit) {
            isChanged = false

            val dialog = AlertDialog.Builder(context)
                    .setTitle(item.title)
                    .setView(R.layout.dialog_anime_info)
                    .setNegativeButton(android.R.string.ok) { _,_ ->
                        if (isChanged) {
                            onEdit(item, true)
                        }
                    }
                    .setPositiveButton(R.string.edit) { _,_ ->
                        onEdit(item, false)
                    }
                    .show()

            dialog.setCanceledOnTouchOutside(false)

            dialog.findViewById<View>(android.R.id.content).setBackgroundColor(Color.WHITE)

            val textTitleOther: TextView? = dialog.findViewById(R.id.text_title_other)
            if (item.titleOther.isNullOrEmpty()) {
                textTitleOther?.visibility = View.GONE
            }
            else {
                textTitleOther?.visibility = View.VISIBLE
                textTitleOther?.text = item.titleOther
            }

            val textTotal: EditText? = dialog.findViewById(R.id.text_total)
            textTotal?.setText(if (item.total > 0) item.total.toString() else "")

            val editProgress: EditText? = dialog.findViewById(R.id.input_progress)
            editProgress?.setText(
                    if (item.progress > 0)
                        DecimalFormat("###.#").format(item.progress)
                    else "")

            val changedColor = ContextCompat.getColor(context, R.color.colorAccent)

            val btnPlus = dialog.findViewById<View>(R.id.btn_plus_ep)
            btnPlus?.setOnClickListener {
                plusProgressEpisode(editProgress!!)
                item.progress = editProgress.text.toString().toFloatOrNull() ?: 0f

                if (!isChanged) {
                    editProgress.setTextColor(changedColor)
                    isChanged = true
                }
            }
            dialog.findViewById<View>(R.id.btn_plus_ep_area)?.setOnClickListener {
                btnPlus.performClick()
            }

            val btnMinus = dialog.findViewById<View>(R.id.btn_minus_ep)
            btnMinus?.setOnClickListener {
                minusProgressEpisode(editProgress!!)
                item.progress = editProgress.text.toString().toFloatOrNull() ?: 0f

                if (!isChanged) {
                    editProgress.setTextColor(changedColor)
                    isChanged = true
                }
            }
            dialog.findViewById<View>(R.id.btn_minus_ep_area).setOnClickListener {
                btnMinus.performClick()
            }

            val textMedia: TextView? = dialog.findViewById(R.id.text_media)
            textMedia?.text = item.getMediaText()

            val textStartDate: TextView? = dialog.findViewById(R.id.text_start_date)
            textStartDate?.text =
                    item.startDate.toFormattedString(
                            context.getString(R.string.format_year),
                            context.getString(R.string.format_month))

            val rating: RatingBar? = dialog.findViewById(R.id.view_rating)
            rating?.rating = item.ratingAsFloat * 5f

            val memo: EditText? = dialog.findViewById(R.id.view_memo)
            memo?.setText(item.memo)

            val link: TextView? = dialog.findViewById(R.id.text_url)
            link?.text = item.link
        }

    }

}