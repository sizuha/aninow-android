package jp.co.sizuha.atelier.aninow.ui

import android.widget.EditText


fun plusProgressEpisode(editText: EditText) {
    val p = (editText.text.toString().toFloatOrNull() ?: 0f).toInt() + 1
    editText.setText(p.toString())
}

fun minusProgressEpisode(editText: EditText) {
    val p = (editText.text.toString().toFloatOrNull() ?: 0f).toInt() - 1
    editText.setText(if (p > 0) p.toString() else "")
}