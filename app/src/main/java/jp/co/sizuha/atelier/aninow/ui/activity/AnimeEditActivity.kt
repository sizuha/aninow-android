package jp.co.sizuha.atelier.aninow.ui.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import jp.co.sizuha.atelier.aninow.R
import jp.co.sizuha.atelier.aninow.model.Anime
import jp.co.sizuha.atelier.aninow.model.OptionalYearMonth
import jp.co.sizuha.atelier.aninow.model.MediaType
import jp.co.sizuha.atelier.aninow.ui.adapter.MonthAdapter
import jp.co.sizuha.atelier.aninow.ui.adapter.YearAdapter
import jp.co.sizuha.atelier.aninow.ui.minusProgressEpisode
import jp.co.sizuha.atelier.aninow.ui.plusProgressEpisode
import kotlinx.android.synthetic.main.activity_anime_edit.*
import sizuha.library.slib.util.Alert
import java.text.DecimalFormat
import java.util.*

class AnimeEditActivity : AppCompatActivity() {

    companion object {

        fun startActivityForResult(from: Activity, requestCode: Int, item: Anime? = null) {
            val intent = Intent(from, AnimeEditActivity::class.java)
            if (item != null) intent.putExtra("item", item)

            from.startActivityForResult(intent, requestCode)
        }

    }

    private lateinit var orgItem: Anime
    private val afterItem: Anime = Anime()

    private var isDataChanged = false
    private var actionSave: MenuItem? = null

    private lateinit var yearAdapter: YearAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anime_edit)
        setResult(Activity.RESULT_CANCELED)

        orgItem = intent.getParcelableExtra("item") ?: Anime()
        val isNewItem = orgItem.idx < 0

        if (isNewItem) {
            orgItem.startDate.from(Calendar.getInstance())
        }
        afterItem.idx = orgItem.idx
        afterItem.startDate = orgItem.startDate

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title =
                if (isNewItem)
                    getString(R.string.txt_new_item)
                else
                    getString(R.string.txt_edit_item) + orgItem.title

        area_start_date_set.visibility = View.GONE

        val today = OptionalYearMonth().from(Calendar.getInstance())
        yearAdapter = YearAdapter(this, 1950, today.year)
        select_startdate_year.adapter = yearAdapter
        select_startdate_month.adapter = MonthAdapter(this)

        select_media.adapter = ArrayAdapter(this,
                R.layout.adapter_simple_text,
                R.id.text_display,
                resources.getStringArray(R.array.array_media))

        displayData()
        initUIEvents()

        isDataChanged = isNewItem
    }

    override fun onBackPressed() {
        if (!isDataChanged) applyInputData()

        if (isDataChanged) {
            Alert.confirm(this, getString(R.string.msg_confirm_without_save)) { _, _ ->
                finish()
            }
            return
        }

        super.onBackPressed()
    }

    private fun toEditText(rawVal: Int): String = if (rawVal > 0) rawVal.toString() else ""
    private fun toEditText(rawVal: Float): String = if (rawVal > 0f) DecimalFormat("###.#").format(rawVal) else ""

    private fun displayData() {
        input_title.setText(orgItem.title)
        input_title_other.setText(orgItem.titleOther)

        input_total.setText(toEditText(orgItem.total))
        input_progress.setText(toEditText(orgItem.progress))

        displayStartDate()
        select_startdate_year.setSelection(yearAdapter.getItemPosition(orgItem.startDate.year))
        select_startdate_month.setSelection(orgItem.startDate.month)

        select_media.setSelection(orgItem.media.ordinal)

        input_rating.rating = 5f * orgItem.rating /100f

        check_finished.isChecked = orgItem.finished

        input_link.setText(orgItem.link)
        input_memo.setText(orgItem.memo)
    }

    private fun applyInputData() {
        afterItem.title = input_title.text.toString()
        afterItem.titleOther = input_title_other.text.toString()

        afterItem.total = input_total.text.toString().toIntOrNull() ?: 0
        afterItem.progress = input_progress.text.toString().toFloatOrNull() ?: 0f

        applyStartDate()

        val selectedMediaPos = select_media.selectedItemPosition
        afterItem.media = MediaType.values()[selectedMediaPos]

        afterItem.rating = (input_rating.rating/5f * 100f).toInt()
        afterItem.finished = check_finished.isChecked
        afterItem.link = input_link.text.toString()
        afterItem.memo = input_memo.text.toString()

        isDataChanged = isDataChanged || afterItem != orgItem
    }

    private fun applyStartDate() {
        afterItem.startDate.year = select_startdate_year.selectedItemId.toInt()
        afterItem.startDate.month = select_startdate_month.selectedItemPosition
    }

    private fun displayStartDate() {
        view_start_date.text = afterItem.startDate.toFormattedString(getString(R.string.format_year), getString(R.string.format_month))
    }

    private fun initUIEvents() {
        area_check_finished.setOnClickListener { check_finished.performClick() }
        area_select_media.setOnClickListener { select_media.performClick() }
        area_start_date.setOnClickListener { toggleStartDateSetControl() }

        select_startdate_year.onItemSelectedListener = onDateSelectedListener
        select_startdate_month.onItemSelectedListener = onDateSelectedListener

        btn_minus_ep.setOnClickListener { minusProgressEpisode(input_progress) }
        btn_minus_ep_area.setOnClickListener { btn_minus_ep.performClick() }

        btn_plus_ep.setOnClickListener { plusProgressEpisode(input_progress) }
        btn_plus_ep_area.setOnClickListener { btn_plus_ep.performClick() }
    }

    inner class OnDateSelectedListener : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            applyStartDate()
            displayStartDate()
            isDataChanged = true
        }
    }
    private val onDateSelectedListener = OnDateSelectedListener()

    private fun toggleStartDateSetControl() {
        val isShown = area_start_date_set.visibility == View.VISIBLE

        val textColor = ContextCompat.getColor(this, if (isShown) R.color.input_text else R.color.colorAccent)
        area_start_date_set.visibility = if (isShown) View.GONE else View.VISIBLE

        view_start_date.setTextColor(textColor)
    }


    private fun checkValidation(): Boolean {
        if (afterItem.title.isNullOrEmpty()) {
            input_title.error = getString(R.string.err_empty_require_field)
            input_title.requestFocus()
            return false
        }

        return true
    }

    private fun saveAndClose() {
        applyInputData()
        if (!checkValidation()) {
            return
        }

        val data = Intent()
        data.putExtra("item", afterItem)
        setResult(Activity.RESULT_OK, data)

        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.item_detail_tools, menu)

        actionSave = menu?.findItem(R.id.action_save)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_save -> {
                saveAndClose()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

}
