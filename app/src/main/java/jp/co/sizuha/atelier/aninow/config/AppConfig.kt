package jp.co.sizuha.atelier.aninow.config

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import sizuha.library.slib.util.SystemUtil.AppPermission


const val SHOW_DEBUG_LOG = true
const val APP_TAG = "AniNow"
const val USER_DB_VER = 1

val usePermissions: List<AppPermission>
    get() = listOf(
            AppPermission(READ_EXTERNAL_STORAGE, ""),
            AppPermission(WRITE_EXTERNAL_STORAGE, "")
    )