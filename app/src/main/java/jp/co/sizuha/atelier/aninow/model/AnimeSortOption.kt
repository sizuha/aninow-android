package jp.co.sizuha.atelier.aninow.model

import android.content.Context
import android.os.Bundle
import jp.co.sizuha.atelier.aninow.R

class AnimeSortOption {

    enum class Field {
        TITLE, START_DATE, MEDIA, RATING
    }

    var targetField = Field.TITLE
    var asc = true

    val asDbColumn: String
        get() = when (targetField) {
            Field.START_DATE -> "start_date"
            Field.MEDIA -> "media"
            Field.RATING -> "rating"
            else -> "title"
        }

    fun load(bundle: Bundle?) {
        targetField = Field.values()[ bundle?.getInt("sort_field", 0) ?: 0]
        asc = bundle?.getBoolean("sort_asc", true) ?: true
    }

    fun writeTo(bundle: Bundle?) {
        bundle?.putInt("sort_field", targetField.ordinal)
        bundle?.putBoolean("sort_asc", asc)
    }

    fun getSortFieldText(context: Context): String {
        return context.getString(when (targetField) {
            Field.START_DATE -> R.string.field_pub_date
            Field.MEDIA -> R.string.field_media
            Field.RATING -> R.string.field_rating
            else -> R.string.field_title
        })
    }

}