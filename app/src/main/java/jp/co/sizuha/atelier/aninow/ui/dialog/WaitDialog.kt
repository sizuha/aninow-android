package jp.co.sizuha.atelier.aninow.ui.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.KeyEvent
import android.view.Window
import jp.co.sizuha.atelier.aninow.R

class WaitDialog {

    companion object {

        fun show(context: Context): AlertDialog {
            val dialog = AlertDialog.Builder(context)
                    .setView(R.layout.dialog_wait)
                    .setOnKeyListener { _, keyCode, _ ->
                        when (keyCode) {
                            KeyEvent.KEYCODE_BACK -> true
                            else -> false
                        }
                    }
                    .create()

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()

            return dialog
        }

    }

}