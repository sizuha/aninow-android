package jp.co.sizuha.atelier.aninow

import android.content.ContentValues
import com.kishe.sizuha.kotlin.squery.SQuery
import jp.co.sizuha.atelier.aninow.model.Anime
import jp.co.sizuha.atelier.aninow.model.MediaType
import sizuha.library.slib.util.CsvParser
import sizuha.library.slib.util.Logger
import java.io.*

class BackupManager {

    private val tableAnime = Anime()

    private fun markRemovedAll(db: SQuery) {
        val param = ContentValues().apply { put("removed", 1) }
        db.from(Anime.tableName).update(param)
    }

    private fun removeMarkedItems(db: SQuery) {
        db.from(Anime.tableName).where("removed=1").delete()
    }

    private fun rollback(db: SQuery) {
        db.from(Anime.tableName).where("removed=0").delete()

        val param = ContentValues().apply { put("removed", 0) }
        db.from(Anime.tableName).where("removed=1").update(param)

    }

    var isClearMode = false
    private var lastItem: Anime? = null

    fun importCSV(db: SQuery, filepath: String, clearCurrentData: Boolean = false): Boolean {
        Logger.i("load csv: $filepath")

        isClearMode = clearCurrentData
        if (isClearMode) markRemovedAll(db)

        val parser = CsvParser(1)

        lastItem = null

        var input: FileInputStream? = null
        try {
            input = FileInputStream(filepath)

            //0     1           2     3                  4               5     6   7           8    9
            //title,title_other,media,start_date(yyyyMM),progress(float),total,fin,rating(0~5),memo,link
            parser.parse(input) {
                Logger.d("-> parsed col[" + it.col + "]: " + it.data)

                when (it.col) {
                    0 -> {
                        saveItem(db, lastItem)
                        lastItem = Anime()
                        lastItem?.title = it.data.trim()
                    }
                    1 -> lastItem?.titleOther = it.data
                    2 -> lastItem?.media = parseMediaText(it.data)
                    3 -> lastItem?.startDate?.from(it.asInt ?: 0)
                    4 -> lastItem?.progress = it.asFloat ?: 0f
                    5 -> lastItem?.total = it.asInt ?: 0
                    6 -> lastItem?.finished = it.asBoolean
                    7 -> lastItem?.rating = (it.asInt ?: 0) * (100/5)
                    8 -> lastItem?.memo = it.data
                    9 -> lastItem?.link = it.data
                }
            }

            saveItem(db, lastItem)
        }
        catch (e: FileNotFoundException) {
            if (isClearMode) rollback(db)
            return false
        }
        finally {
            input?.close()
        }

        if (isClearMode) removeMarkedItems(db)
        return true
    }

    private fun parseMediaText(str: String): MediaType =
        when (str.toUpperCase()) {
            "MOVIE" -> MediaType.MOVIE
            "OVA" -> MediaType.OVA
            "TV" -> MediaType.TV
            else -> MediaType.NONE
        }

    private fun convertCsvText(str: String, isLastCol: Boolean = false): String {
        val cellData =
                if (str.isEmpty()) ""
                else "\"" + (str.replace("\"", "\"\"")) + "\""

        return cellData + if (isLastCol) "" else ","
    }

    fun exportCSV(db: SQuery, outputPath: String): Boolean {
        var output: FileOutputStream? = null

        try {
            output = FileOutputStream(outputPath)
            output.bufferedWriter().use { writer ->
                writer.write("title,title_other,media,start_date,progress,total,fin,rating,memo,link\n")

                db.from(Anime.tableName).selectForEach({ Anime() }) {
                    val csvRow = StringBuilder().run {
                        append( convertCsvText(it.title) )
                        append( convertCsvText(it.titleOther) )
                        append( convertCsvText(it.media.toString()) )
                        append( convertCsvText(it.startDate.toString()) )
                        append( convertCsvText(it.progress.toString()) )
                        append( convertCsvText(it.total.toString()) )
                        append( convertCsvText( (if (it.finished) 1 else 0).toString() ) )
                        append( convertCsvText((it.ratingAsFloat * 5).toInt().toString()) )
                        append( convertCsvText(it.memo) )
                        append( convertCsvText(it.link, true) )
                        Unit
                    }

                    writer.write(csvRow.toString() + "\n")
                    Logger.d(csvRow.toString())
                    writer.flush()
                }
            }
        }
        catch (e: IOException) {
            return false
        }
        finally {
            output?.close()
        }

        return true
    }

    private fun saveItem(db: SQuery, item: Anime?) {
        if (item == null || item.title.isEmpty()) return

        db.from(Anime.tableName).insert(item)
    }

}