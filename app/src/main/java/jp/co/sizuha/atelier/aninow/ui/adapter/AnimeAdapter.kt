package jp.co.sizuha.atelier.aninow.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import com.daimajia.swipe.adapters.BaseSwipeAdapter
import jp.co.sizuha.atelier.aninow.R
import jp.co.sizuha.atelier.aninow.config.SHOW_DEBUG_LOG
import jp.co.sizuha.atelier.aninow.model.Anime
import jp.co.sizuha.atelier.aninow.model.MediaType
import sizuha.library.slib.util.Logger
import java.text.DecimalFormat


class AnimeAdapter(private val context: Context) : BaseSwipeAdapter() {

    var onItemClickListener: OnItemClick? = null
    var items: MutableList<Anime>? = null

    private val inflater = LayoutInflater.from(context)

    override fun getItem(position: Int): Any = items?.get(position)!!
    override fun getSwipeLayoutResourceId(position: Int): Int = R.id.swipe_layout
    override fun getItemId(position: Int): Long = items?.get(position)?.idx?.toLong()!!
    override fun getCount(): Int = items?.count() ?: 0

    override fun generateView(position: Int, parent: ViewGroup?): View {
        val itemView = inflater.inflate(R.layout.adapter_anime_list_item, parent, false)
        val viewHolder = ViewHolder(itemView)
        itemView.tag = viewHolder

        return itemView
    }

    override fun fillValues(position: Int, convertView: View?) {
        val viewHolder = convertView?.tag as ViewHolder

        // データ表示
        val item = items?.get(position)!!

        viewHolder.textTitle?.text = item.title
        viewHolder.textTitleOther?.text = item.titleOther
        viewHolder.textTitleOther?.visibility = if (item.titleOther.isNullOrEmpty()) View.GONE else View.VISIBLE
        viewHolder.rating?.rating = item.ratingAsFloat * 5f

        viewHolder.textMedia?.text = item.getMediaText()
        viewHolder.textMedia?.visibility =
                if (item.media == MediaType.NONE) View.INVISIBLE else View.VISIBLE

        viewHolder.textStartDate?.text =
                item.startDate.toFormattedString(context.getString(R.string.format_year), context.getString(R.string.format_month))

        var progressMessage =
                if (item.progress >= 0)
                    String.format(context.getString(R.string.format_progress),
                            DecimalFormat("###.#").format(item.progress))
                else ""

        if (!progressMessage.isEmpty()) progressMessage += " / "

        progressMessage +=
                if (item.total > 0)
                    String.format(context.getString(R.string.format_end_ep), item.total)
                else
                    context.getString(R.string.format_end_ep_unknown)

        viewHolder.textProgress?.text = progressMessage

        viewHolder.btnMoveToFin?.visibility =
                if (item.finished) View.GONE else View.VISIBLE

        // クリック処理
        viewHolder.btnMoveToFin?.setOnClickListener { onItemClickListener?.onFinishedClick(item, position) }
        viewHolder.btnRemove?.setOnClickListener { onItemClickListener?.onRemoveClick(item, position) }
        viewHolder.btnEdit?.setOnClickListener { onItemClickListener?.onEditClick(item, position) }
        viewHolder.contentView?.setOnClickListener { onItemClickListener?.onItemClick(item, position) }
    }

    inner class ViewHolder(itemView: View) {
        val contentView: View? = itemView.findViewById(R.id.area_content)

        val textTitle: TextView? = itemView.findViewById(R.id.text_title)
        val textTitleOther: TextView? = itemView.findViewById(R.id.text_title_other)
        val rating: RatingBar? = itemView.findViewById(R.id.view_rating)
        val textProgress: TextView? = itemView.findViewById(R.id.text_progress)
        val textMedia: TextView? = itemView.findViewById(R.id.text_media_type)
        val textStartDate: TextView? = itemView.findViewById(R.id.text_start_date)

        val btnMoveToFin: View? = itemView.findViewById(R.id.button_move_to_fin)
        val btnEdit: View? = itemView.findViewById(R.id.button_edit)
        val btnRemove: View? = itemView.findViewById(R.id.button_remove)
    }

    interface OnItemClick {
        fun onItemClick(item: Anime, position: Int)
        fun onRemoveClick(item: Anime, position: Int)
        fun onFinishedClick(item: Anime, position: Int)
        fun onEditClick(item: Anime, position: Int)
    }

}