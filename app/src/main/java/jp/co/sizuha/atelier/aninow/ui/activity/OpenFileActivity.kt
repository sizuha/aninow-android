package jp.co.sizuha.atelier.aninow.ui.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import jp.co.sizuha.atelier.aninow.R
import kotlinx.android.synthetic.main.activity_open_file.*
import sizuha.library.slib.util.DataUtil
import sizuha.library.slib.util.Logger
import java.io.File
import java.util.*

class OpenFileActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    companion object {

        const val RES_FIELD_PULL_FILEPATH: String = "path"
        const val RES_FIELD_FILENAME: String = "filename"

        fun startActivityForResult(from: Activity, requestCode: Int, title: String, extList: List<String>? = null ) {
            val intent = Intent(from, OpenFileActivity::class.java)
            intent.putExtra("title", title)

            if (extList?.isNotEmpty() == true) {
                intent.putExtra("filter", DataUtil.stringListToString(extList))
            }

            from.startActivityForResult(intent, requestCode)
        }

    }

    private var listItems = ArrayList<String>()
    private var adapter: ArrayAdapter<String>? = null

    private var currentPath: String? = null
    private var selectedFilePath: String? = null /* Full path, i.e. /mnt/sdcard/folder/file.txt */
    private var selectedFileName: String? = null /* File Name Only, i.e file.txt */

    private var filter: List<String>? = null


    private val rootPath: String
        get() = Environment.getExternalStorageDirectory().absolutePath + "/"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_file)
        setResult(Activity.RESULT_CANCELED)

        supportActionBar?.title = intent.getStringExtra("title") ?: ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        filter = (intent.getStringExtra("filter") ?: "").split(',')

        lvList?.onItemClickListener = this
        setCurrentPath(rootPath)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setCurrentPath(path: String) {
        Logger.d("setCurrentPath: " + path)

        currentPath = path
        text_current_path.text = path

        val folders = ArrayList<String>()
        val files = ArrayList<String>()
        val allEntries = File(path).listFiles { file ->
            file.isDirectory || filter?.contains(file.extension.toLowerCase()) ?: true
        }

        Logger.d("count of allEntries: " + allEntries.size)

        if (path != rootPath) {
            folders.add("..")
        }

        allEntries.forEach {
            if (it.isDirectory) {
                folders.add(it.name)
            }
            else if (it.isFile) {
                files.add(it.name)
            }
        }

        Logger.d("count of folders: " + folders.size)
        Logger.d("count of files: " + files.size)

        Collections.sort(folders) { s1, s2 -> s1.compareTo(s2, ignoreCase = true) }
        Collections.sort(files) { s1, s2 -> s1.compareTo(s2, ignoreCase = true) }

        listItems.clear()

        folders.forEach { listItems.add(it + "/") }
        files.forEach { listItems.add(it) }

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        lvList?.adapter = adapter

        adapter?.notifyDataSetChanged()

        Logger.d("count of listItems: " + listItems.size)
    }

    override fun onBackPressed() {
        if (!moveToParentDir()) super.onBackPressed()
    }

    private fun moveToParentDir(): Boolean {
        if (currentPath != Environment.getExternalStorageDirectory().absolutePath + "/") {
            setCurrentPath(File(currentPath).parent + "/")
            return true
        }

        return false
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val entryName = parent?.getItemAtPosition(position) as String
        when {
            entryName.endsWith("../") -> {
                moveToParentDir()
            }
            entryName.endsWith("/") -> setCurrentPath(currentPath + entryName)
            else -> {
                selectedFilePath = currentPath + entryName
                selectedFileName = entryName

                Logger.i("file path: " + selectedFilePath)
                Logger.i("file name: " + selectedFileName)

                val result = Intent()
                result.putExtra(RES_FIELD_PULL_FILEPATH, selectedFilePath)
                result.putExtra(RES_FIELD_FILENAME, selectedFileName)
                setResult(Activity.RESULT_OK, result)
                finish()
            }
        }
    }
}
