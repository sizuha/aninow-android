package jp.co.sizuha.atelier.aninow.model

enum class MediaType(val rawValue :Int) {

    NONE(0), MOVIE(1), OVA(2), TV(3)

}