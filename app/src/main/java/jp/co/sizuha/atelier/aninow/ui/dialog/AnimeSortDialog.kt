package jp.co.sizuha.atelier.aninow.ui.dialog

import android.app.AlertDialog
import android.content.Context
import android.widget.RadioButton
import jp.co.sizuha.atelier.aninow.R
import jp.co.sizuha.atelier.aninow.model.AnimeSortOption

class AnimeSortDialog {

    companion object {

        fun show(context: Context, sortOption: AnimeSortOption, onDone: () -> Unit) {
            val dialog = AlertDialog.Builder(context)
                    .setTitle(null)
                    .setView(R.layout.dialog_anime_sort)
                    .setNegativeButton(android.R.string.cancel) { _,_ ->
                        onDone()
                    }
                    .setPositiveButton(android.R.string.ok) { _,_ ->
                        onDone()
                    }
                    .show()

            val byTitle: RadioButton = dialog.findViewById(R.id.sort_by_title)
            initFieldRadioButton(byTitle, sortOption, AnimeSortOption.Field.TITLE)

            val byDate: RadioButton = dialog.findViewById(R.id.sort_by_date)
            initFieldRadioButton(byDate, sortOption, AnimeSortOption.Field.START_DATE)

            val byMedia: RadioButton = dialog.findViewById(R.id.sort_by_media)
            initFieldRadioButton(byMedia, sortOption, AnimeSortOption.Field.MEDIA)

            val byRating: RadioButton = dialog.findViewById(R.id.sort_by_rating)
            initFieldRadioButton(byRating, sortOption, AnimeSortOption.Field.RATING)

            val optionAsc: RadioButton = dialog.findViewById(R.id.sort_asc)
            optionAsc.isChecked = sortOption.asc
            optionAsc.setOnCheckedChangeListener { _, isChecked -> sortOption.asc = isChecked }

            val optionDesc: RadioButton = dialog.findViewById(R.id.sort_desc)
            optionDesc.isChecked = !sortOption.asc
            optionDesc.setOnCheckedChangeListener { _, isChecked -> sortOption.asc = !isChecked }
        }

        private fun initFieldRadioButton(btn: RadioButton, sortOption: AnimeSortOption, field: AnimeSortOption.Field) {
            btn.isChecked = sortOption.targetField == field
            btn.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) sortOption.targetField = field
            }
        }

    }

}