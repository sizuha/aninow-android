package jp.co.sizuha.atelier.aninow.model

import android.os.Parcel
import android.os.Parcelable
import com.kishe.sizuha.kotlin.squery.Column
import com.kishe.sizuha.kotlin.squery.DateType
import com.kishe.sizuha.kotlin.squery.ISQueryRow
import com.kishe.sizuha.kotlin.squery.PrimaryKey

class Anime() : ISQueryRow, Parcelable {

    companion object CREATOR : Parcelable.Creator<Anime> {
        val tableName = "anime"

        override fun createFromParcel(parcel: Parcel): Anime {
            return Anime(parcel)
        }

        override fun newArray(size: Int): Array<Anime?> {
            return arrayOfNulls(size)
        }
    }

    @Column("idx")
    @PrimaryKey(autoInc = true)
    var idx = -1

    @Column("title", notNull = true)
    var title = ""

    @Column("title_other")
    var titleOther = ""

    var startDate = OptionalYearMonth()

    @Column("start_date")
    private var startDateRaw: Int
        get() = startDate.toInt()
        set(value) {
            startDate.from(value)
        }

    var media = MediaType.NONE

    @Column("media")
    private var mediaRaw: Int
        get() = media.rawValue
        set(value) {
            media = MediaType.values().first { it.rawValue == value }
        }

    fun getMediaText(): String = when (media) {
        MediaType.MOVIE -> "Movie"
        MediaType.OVA -> "OVA"
        MediaType.TV -> "TV"
        else -> ""
    }

    var progress: Float = 0f

    @Column("progress")
    private var progressRaw: Int
        get() = (progress*10f).toInt()
        set(value) {
            progress = value / 10f
        }

    @Column("total")
    var total = 0

    @Column("fin")
    var finished = false

    @Column("rating")
    var rating = 0 // 0 ~ 100

    val ratingAsFloat: Float
        get() = rating / 100f

    @Column("memo")
    var memo = ""

    @Column("link")
    var link = ""

    @Column("img_path")
    var imagePath = ""

    @Column("removed")
    var removed = false

    constructor(parcel: Parcel) : this() {
        idx = parcel.readInt()
        title = parcel.readString() ?: ""
        titleOther = parcel.readString() ?: ""
        progress = parcel.readFloat()
        total = parcel.readInt()
        finished = parcel.readInt() == 1
        rating = parcel.readInt()
        memo = parcel.readString() ?: ""
        link = parcel.readString() ?: ""
        imagePath = parcel.readString() ?: ""
        startDate = OptionalYearMonth(parcel.readInt())
        media = MediaType.values()[parcel.readInt()]
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idx)
        parcel.writeString(title)
        parcel.writeString(titleOther)
        parcel.writeFloat(progress)
        parcel.writeInt(total)
        parcel.writeInt(if (finished) 1 else 0)
        parcel.writeInt(rating)
        parcel.writeString(memo)
        parcel.writeString(link)
        parcel.writeString(imagePath)
        parcel.writeInt(startDate.toInt())
        parcel.writeInt(media.rawValue)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean =
        if (other is Anime) {
            title == other.title &&
            titleOther == other.titleOther &&
            startDate == other.startDate &&
            media == other.media &&
            progress.toString() == other.progress.toString() &&
            total == other.total &&
            rating == other.rating &&
            finished == other.finished &&
            memo == other.memo &&
            link == other.link
        }
        else super.equals(other)

    override fun hashCode(): Int {
        var result = idx
        result = 31 * result + title.hashCode()
        result = 31 * result + titleOther.hashCode()
        result = 31 * result + startDate.hashCode()
        result = 31 * result + media.hashCode()
        result = 31 * result + progress.hashCode()
        result = 31 * result + total
        result = 31 * result + finished.hashCode()
        result = 31 * result + rating
        result = 31 * result + memo.hashCode()
        result = 31 * result + link.hashCode()
        result = 31 * result + imagePath.hashCode()
        result = 31 * result + removed.hashCode()
        return result
    }

}